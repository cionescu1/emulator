module emu() ;
reg [ 31 : 0 ] r [ 0 : 15 ] ; // register file.
reg [ 31 : 0 ] memory [ 0 : 1023 ] ; // memory.
reg [ 32 : 0 ] temp;
reg [ 63 : 0 ] temp_mulr;
reg [ 31 : 0 ] actual;
reg [ 31 : 0 ] imm32;
integer i;
reg clock=0;
reg N;
reg Z;
reg C;
reg V;

//r[15] is the PC
//r[14] is the Link Register
//r[13] is the Stack Pointer

task addi;
    input [2:0] rdn;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: addi with rdn = %h, imm8 = %h",rdn,imm8);
        temp = r[rdn] + imm8;
        V=0;
        if (temp[31]==1 && r[rdn][31]==0)
            V = 1;
        r[rdn] = r[rdn] + imm8;
        C = temp[32];
        N = r[rdn][31];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task addr;
    input [2:0] rm;
    input [2:0] rn;
    input [2:0] rd;
    begin
        $display("Decoded instruction: addr with rm = %h, rn = %h, rd = %h",rm,rn,rd);
        temp = r[rm] + r[rn];
        V = 0;
        if(temp[31] == 1 && r[rm][31] == 0 && r[rn][31] == 0)
            V = 1;
        if(temp[31] == 0 && r[rm][31] == 1 && r[rn][31] == 1)
            V = 1;
        r[rd] = r[rm] + r[rn];
        C = temp[32];
        N = r[rd][31];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task  addspi;
    input [2:0] rdn;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: addspi with rdn = %h, imm8 = %h",rdn,imm8);
        r[rdn] = r[13] + imm8*4;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task incsp ;
    input [6:0] imm7;
    begin
        $display("Decoded instruction: incsp with imm7 = %h",imm7);
        r[13] = r[13] + imm7*4;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task addpci ;
    input [2:0] rd;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: addpci with rd = %h, imm8 = %h",rd,imm8);
        r[rd] = r[15] + imm8*4;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task subi ;
    input [2:0] rdn;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: subi with rdn = %h, imm8 = %h",rdn,imm8);
        temp = r[rdn] + imm8;
        V = 0;
        if (temp[31]==1 && r[rdn][31]==0)
            V = 1;
        r[rdn] = r[rdn] - imm8;
        C = temp[32];
        N = r[rdn][31];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task subr ;
    input [2:0] rm;
    input [2:0] rn;
    input [2:0] rd;
    begin
        $display("Decoded instruction: subr with rm = %h, rn = %h, rd = %h",rm,rn,rd);
        temp = r[rn] - r[rm];
        V = 0;
        if(temp[31] == 1 && r[rm][31] == 0 && r[rn][31] == 0)
            V = 1;
        if(temp[31] == 0 && r[rm][31] == 1 && r[rn][31] == 1)
            V = 1;
        r[rd] = r[rn] - r[rm];
        C = temp[32];
        N = r[rd][31];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task decsp ;
    input [6:0] imm7;
    begin
        $display("Decoded instruction: decsp with imm7 = %h",imm7);
        r[13] = r[13] - imm7*4;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task mulr ;
    input [2:0] rn;
    input [2:0] rdm;
    begin
        $display("Decoded instruction: mulr with rn = %h, rdm = %h",rn,rdm);
        temp_mulr = r[rdm] * r[rn];
        N = temp_mulr[31];
        Z = 0;
        if(temp_mulr == 0)
            Z = 1;
        r[rdm] = r[rdm] * r[rn];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task andr;
    input [2:0] rm;
    input [2:0] rdn;
    begin
        $display("Decoded instruction: andr with rdn = %h, rm = %h",rdn,rm);
        temp = r[rdn] & r[rm];
        r[rdn] = r[rdn] & r[rm];
        N = r[rdn][31];
        C = temp[32];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task orr;
    input [2:0] rm;
    input [2:0] rdn;
    begin
        $display("Decoded instruction: orr with rdn = %h, rm = %h",rdn,rm);
        temp = r[rdn] | r[rm];
        r[rdn] = r[rdn] | r[rm];
        N = r[rdn][31];
        C = temp[32];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task eorr;
    input [2:0] rm;
    input [2:0] rdn;
    begin
        $display("Decoded instruction: eorr with rdn = %h, rm = %h",rdn,rm);
        temp = r[rdn] ^ r[rm];
        r[rdn] = r[rdn] ^ r[rm];
        N = r[rdn][31];
        C = temp[32];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task negr;
    input [2:0] rn;
    input [2:0] rd;
    begin
        $display("Decoded instruction: negr with rn = %h, rd = %h",rn,rd);
        temp = r[~rd];
        r[rn] = r[~rd];
        N = r[rn][31];
        C = temp[32];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task lsli;
    input [4:0] imm5;
    input [2:0] rm;
    input [2:0] rd;
    begin
        $display("Decoded instruction: lsli with rd = %h, rm = %h, imm5 = %h",rd,rm,imm5);
        temp = r[rm] << imm5;
        r[rd] = r[rm] << imm5;
        N = r[rd][31];
        C = temp[32];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task lslr;
    input [2:0] rm;
    input [2:0] rdn;
    begin
        $display("Decoded instruction: lslr with rm = %h, rdn = %h",rm,rdn);
        temp = r[rdn] << r[rm];
        r[rdn] = r[rdn] << r[rm];
        N = r[rdn][31];
        C = temp[32];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task lsri;
    input [4:0] imm5;
    input [2:0] rm;
    input [2:0] rd;
    begin
        $display("Decoded instruction: lsri with rd = %h, rm = %h, imm5 = %h",rd,rm,imm5);
        temp = r[rm] >> imm5;
        r[rd] = r[rm] >> imm5;
        N = r[rd][31];
        C = temp[32];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task lsrr;
    input [2:0] rm;
    input [2:0] rdn;
    begin
        $display("Decoded instruction: lslr with rm = %h, rdn = %h",rm,rdn);
        temp = r[rdn] >> r[rm];
        r[rdn] = r[rdn] >> r[rm];
        N = r[rdn][31];
        C = temp[32];
        if(r[rdn] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task asri;
    input [4:0] imm5;
    input [2:0] rm;
    input [2:0] rd;
    begin
        $display("Decoded instruction: asri with rd = %h, rm = %h, imm5 = %h",rd,rm,imm5);
        temp = r[rm] >>> imm5;
        r[rd] = r[rm] >>> imm5;
        N = r[rd][31];
        C = temp[32];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task movi;
    input [2:0] rd;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: movi with rd = %h, imm8 = %h",rd,imm8);
        r[rd] = imm8;
        C = 0;
        N = r[rd][31];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task movnr;
    input [2:0] rm;
    input [2:0] rd;
    begin
        $display("Decoded instruction: movnr with rm = %h, rd = %h",rm,rd);
        r[rd] = ~r[rm];
        C = 0;
        N = r[rd][31];
        if(r[rd] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task movrsp;
    input [2:0] rm;
    begin
        $display("Decoded instruction: movrsp with rm = %h",rm);
        r[rm] = r[13];
        N = r[rm][31];
        if(r[rm] == 0)
            Z = 1;
        else
            Z = 0;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b",r[15],Z,N,C,V);
    end
endtask

task ldri;
    input [4:0] imm5;
    input [2:0] rn;
    input [2:0] rt;
    begin
        $display("Decoded instruction: ldri with rt = %h, rn = %h, i5 = %h", rt, rn, imm5);
        r[rt] = memory[(r[rn] + imm5*4)/4];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task ldrr;
    input [2:0] rm;
    input [2:0] rn;
    input [2:0] rt;
    begin
        $display("Decoded instruction: ldrr with rt = %h, rn = %h, rm = %h", rt, rn, rm);
        r[rt] = memory[(r[rn] + r[rm])/4];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task ldrspi;
    input [2:0] rt;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: ldrspi with rt = %h, i8 = %h", rt, imm8);
        r[rt] = memory[(r[13] + imm8*4)/4];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task ldrpci;
    input [2:0] rd;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: ldrpci with rd = %h, i8 = %h", rd, imm8);
        r[rd] = memory[(r[15] + imm8*4)/4];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task stri;
    input [4:0] imm5;
    input [2:0] rn;
    input [2:0] rt;
    begin
        $display("Decoded instruction: stri with rt = %h, rn = %h, i5 = %h", rt, rn, imm5);
        memory[(r[rn] + imm5*4)/4] = r[rt];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task strr;
    input [2:0] rm;
    input [2:0] rn;
    input [2:0] rt;
    begin
        $display("Decoded instruction: strr with rt = %h, rn = %h, rm = %h", rt, rn, rm);
        memory[(r[rn] + r[rm])/4] = r[rt];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task strspi;
    input [2:0] rt;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: strspi with rt = %h, i8 = %h", rt, imm8);
        memory[(r[13] + imm8*4)/4] = r[rt];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task push;
    begin
        $display("Decoded instruction: push");
        memory[r[13]/4] = r[14];
        r[13] = r[13] - 1;
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task pop;
    begin
        $display("Decoded instruction: pop");
        r[13] = r[13] + 1;
        r[15] = memory[r[13]/4];
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task bu;
    input [10:0] imm11;
    reg signed [10:0] aux;
    begin
    $display("Decoded instruction: bu with i11 = %h", imm11);
    aux = imm11;
    r[15] = r[15] + (aux << 1) + 2;
    $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task bc;
    input [3:0] cond;
    input [7:0] imm8;
    reg ok;
    begin
        $display("Decoded instruction: bc with i8 = %h and cond = %b", imm8, cond);
        ok = 0;
        if(cond == 4'b0000 && Z == 1)
            ok = 1;
        if(cond == 4'b0001 && Z == 0)
            ok = 1;
        if(cond == 4'b1011 && N != V)
        begin
            $display("%h %h %h",cond,N,V);
            ok = 1;
        end
        if(cond == 4'b1100 && Z == 0 && N == V)
            ok = 1;
        if(ok == 1)
        begin
            if(imm8[7] == 0)
                r[15] = r[15] + imm8;
            else
                r[15] = r[15] - (~imm8+1);
        end
        if(cond == 4'b1111)
            svc(imm8);
        $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task bl1;
    input [10:0] imm11;
    begin
        r[14] = r[15];
        r[14][0] = 1;
        if(imm11[10] == 0)
            imm32 = imm11;
        else
            imm32 = ~imm11 +1;
        imm32 = imm32 << 12;
    end
endtask

task bl2;
    input [10:0] imm11;
    begin
        if(imm11[10] == 0)
            imm32 = imm32 + imm11;
        else
            imm32 = imm32 - (~imm11+1);
        r[15] = r[15] + imm32;
    end
endtask

task br;
    input [2:0] rm;
    begin
    $display("Decoded instruction: br with rm = %h", rm);
    r[15] = r[rm];
    $display("pc = %h, Z = %b, N = %b, C = %b, V = %b", r[15], Z, N, C, V);
    end
endtask

task svc ;
    input [7:0] imm8;
    begin
        $display("Decoded instruction: svc with imm8 = %h",imm8);
        if(imm8 >= 0 && imm8 <= 7)
        begin
            $display("r%2d = %4h",imm8,r[imm8]);
        end
        else
        begin
            if (imm8 == 16)
                begin
                    for(i=0;i<16;i=i+1)
                    begin
                        $display("r%2d = %4h",i,r[i]);
                    end

                end
            else
                begin
                    if(imm8 == 100)
                    begin:id3
                        $display("Emulation stopped due to SVC 100");
                        $finish;
                    end
                    else
                    begin
                        if(imm8 == 101)
                        begin
                            for(i = 0; i < 10; i = i + 1)
                            begin
                                $display("%h: %h",(i*4),memory[i]);
                            end
                        end
                    end
                end
        end
    end
endtask

initial
begin
    $readmemh("ControlFlow.emu", memory);
    // for( i = 0; i < 15; i = i + 1)
    // begin
    //     r[i] = 32'b0;
    // end
    r[15] = 4;
end

always #1 clock = !clock ; // simulate the clock

task decode;
input [15:0] ins;
    begin
        casex(ins)
            16'b00110xxxxxxxxxxx : addi(ins[10:8], ins[7:0]);
            16'b0001100xxxxxxxxx : addr(ins[8:6], ins[5:3], ins[2:0]);
            16'b10101xxxxxxxxxxx : addspi(ins[10:8], ins[7:0]);
            16'b101100000xxxxxxx : incsp(ins[6:0]);
            16'b10100xxxxxxxxxxx : addpci(ins[10:8], ins[7:0]);
            16'b00111xxxxxxxxxxx : subi(ins[10:8], ins[7:0]);
            16'b0001101xxxxxxxxx : subr(ins[8:6], ins[5:3], ins[2:0]);
            16'b101100001xxxxxxx : decsp(ins[6:0]);
            16'b0100001101xxxxxx : mulr(ins[5:3], ins[2:0]);

            16'b0100000000xxxxxx : andr(ins[5:3], ins[2:0]);
            16'b0100001100xxxxxx : orr(ins[5:3], ins[2:0]);
            16'b0100000001xxxxxx : eorr(ins[5:3], ins[2:0]);
            16'b0100001001xxxxxx : negr(ins[5:3], ins[2:0]);

            16'b00000xxxxxxxxxxx : lsli(ins[10:6], ins[5:3], ins[2:0]);
            16'b0100000010xxxxxx : lslr(ins[5:3], ins[2:0]);
            16'b00001xxxxxxxxxxx : lsri(ins[10:6], ins[5:3], ins[2:0]);
            16'b0100000011xxxxxx : lsrr(ins[5:3], ins[2:0]);
            16'b00010xxxxxxxxxxx : asri(ins[10:6], ins[5:3], ins[2:0]);

            16'b00100xxxxxxxxxxx : movi(ins[10:8], ins[7:0]);
            16'b0100001111xxxxxx : movnr(ins[5:3], ins[2:0]);
            16'b0100011010xxx101 : movrsp(ins[5:3]);

            16'b01101xxxxxxxxxxx : ldri(ins[10:6], ins[5:3], ins[2:0]);
            16'b0101100xxxxxxxxx : ldrr(ins[8:6], ins[5:3], ins[2:0]);
            16'b10011xxxxxxxxxxx : ldrspi(ins[10:8], ins[7:0]);
            16'b01001xxxxxxxxxxx : ldrpci(ins[10:8], ins[7:0]);

            16'b01100xxxxxxxxxxx : stri(ins[10:6], ins[5:3], ins[2:0]);
            16'b0101000xxxxxxxxx : strr(ins[8:6], ins[5:3], ins[2:0]);
            16'b10010xxxxxxxxxxx : strspi(ins[10:8], ins[7:0]);
            16'b1011010100000000 : push();
            16'b1011110100000000 : pop();

            16'b11011111xxxxxxxx : svc(ins[7:0]);

            16'b11100xxxxxxxxxxx : bu(ins[10:0]);
            16'b1101xxxxxxxxxxxx : bc(ins[11:8], ins[7:0]);
            16'b11110xxxxxxxxxxx : bl1(ins[10:0]);
            16'b11111xxxxxxxxxxx : bl2(ins[10:0]);
            16'b0100011100xxx000 : br(ins[5:3]);

            default : $display("Instruction not found.");
        endcase
    end
endtask

always @ ( posedge clock )
begin
    actual = r[15] - 4;
    if(actual[1] == 0)
    begin
        $display("Executing instruction @ %h: '%b'",actual,memory[actual[31:2]][15:0]);
        decode(memory[actual[31:2]][15:0]);
        svc(8'b10000);
        $display("\n");
    end
    else
    begin
        $display("Executing instruction @ %h: '%b'",actual,memory[actual[31:2]][31:16]);
        decode(memory[actual[31:2]][31:16]);
        svc(8'b10000);
        $display("\n");
    end
    r[15] = r[15] + 2;
end

endmodule
